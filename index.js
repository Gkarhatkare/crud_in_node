const express = require('express');
const mongoose = require('mongoose')
const url = 'mongodb://localhost:27017/EmployeDB'
const app = express();
mongoose.connect(url);
const employeeRoute = require('./src/routes/Employee.route')
const productRoute = require('./src/routes/product.route')
const categoryRoute = require('./src/routes/category.route')
const cartRoute = require('./src/routes/cart.route')
const loginRoute = require('./src/routes/login.route') 
const conn= mongoose.connection
conn.on('open', ()=>{
    console.log('connected++++++++')
});

app.use(express.json());
app.use('/employee',employeeRoute);
app.use('/product',productRoute);
app.use('/category',categoryRoute);
app.use('/cart',cartRoute);
app.use('/login',loginRoute);

app.listen(3008,()=>{
    console.log('server run on port 3008');
}) 