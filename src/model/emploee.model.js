const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({
    name :{
        type : String,
        required: true
    },
    mobile:{
        type: Number
    },
    tech:{
        type: String
    },
    email:{
        type: String
    },
    password:{
        type:String,
        required: true
    },
    id: {
        type: String
    },
    createdAt:{
        type:Date,
        default:Date.now
    }

})

module.exports = mongoose.model('employee',employeeSchema);