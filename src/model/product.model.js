const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    name :{
        type : String,
        require: true
    },
    category:{
        type: String
    },
    price:{
        type: Number
    },
    stock:{
        type: Number
    },
    id: {
        type: Number
    }

})

module.exports = mongoose.model('product',productSchema);