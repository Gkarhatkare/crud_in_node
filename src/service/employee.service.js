const Validator= require('validator');
const isEmpty = require('is-empty');

const validateRegisterInput = (employee) =>{
    console.log('employee+++++++',employee);
    let errors = {};
    employee.name = !isEmpty(employee.name) ? employee.name : "";
    employee.mobile = !isEmpty(employee.mobile) ? employee.mobile : "";
    employee.tech = !isEmpty(employee.tech) ? employee.tech : "";
    employee.email = !isEmpty(employee.email) ? employee.email : "";
    employee.password = !isEmpty(employee.password) ? employee.password : "";
    employee.password2 = !isEmpty(employee.password2) ? employee.password2 : ""

    // Name checks
  if (Validator.isEmpty(employee.name)) {
    errors.name = "Name field is required";
  }  
  if (Validator.isEmpty(employee.tech)) {
    errors.tech = "Password field is required";
  }
  if (Validator.isEmpty(employee.mobile)) {
    errors.mobile = "Mobile field is required";
  } 
// Email checks
  if (Validator.isEmpty(employee.email)) {
    errors.email = "Email field is required";
  } else if (!Validator.isEmail(employee.email)) {
    errors.email = "Email is invalid";
  }
// Password checks
  if (Validator.isEmpty(employee.password)) {
    errors.password = "Password field is required";
  }
if (Validator.isEmpty(employee.password2)) {
    errors.password2 = "Confirm password field is required";
  }
if (!Validator.isLength(employee.password, { min: 6, max: 30 })) {
    errors.password = "Password must be at least 6 characters";
  }
if (!Validator.equals(employee.password, employee.password2)) {
    errors.password2 = "Passwords must match";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };

}
module.exports = validateRegisterInput;