const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const loginSch = require('../model/login.model')
const validateLoginInput = require('../service/login.service')
const emploeesData = mongoose.model('employee')
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const Keys = require('../keys')
router.post('/',async(req,resp)=>{
    console.log('login call');
    const { errors, isValid } = validateLoginInput(req.body);

    if(!isValid){
        resp.status(400).json(errors)
    }
    // const loginInfo = new loginSch({
        email = req.body.email
        password = req.body.password
    // })

  emploeesData.findOne({email}).then(employe =>{
    if(!employe){
        resp.status(400).json({email : "Not found"})
    }

    bcrypt.compare(password, employe.password).then(isMatch =>{
        if(isMatch){
            console.log('password match');
           const payload = {
            id: employe.id,
            name: employe.name,
            mobile:employe.mobile,
            tech:employe.tech
           }
        //    resp.json('Login success')

           jwt.sign(
            payload,
            Keys.secretOrKey,
            {
              expiresIn: 31556926 // 1 year in seconds
            }, (err, token)=>{
            resp.json({
                token: "Bearer" + token
            })
           })
        }else{
            resp.status(400).json({password: "Incorrect password"})
        }
    })
  })


    // const data = await loginInfo.save()
    // resp.json(data)
})

module.exports = router;