const express = require('express');
const router = express.Router();
const categorySch = require('../model/category.model')

console.log('categorySch',categorySch);


router.get('/',async(req,resp)=>{
    try{
        const category = await categorySch.find()
        console.log('get all productt', category);
        resp.json(category)

    }catch(err){
        resp.send(err)
    }
})  

router.get('/:id',async(req,resp)=>{
    try{
        const category = await categorySch.findById(req.params.id)
        console.log('get all productt', category);
        resp.json(category)

    }catch(err){
        resp.send(err)
    }
}) 

router.post('/',async(req, resp)=>{
    // console.log('post req');
    console.log('req.body',req.body); 
    const categoryDetail = new categorySch({
        name:req.body.name,
    })
    console.log('categoryDetail++++',categoryDetail);
    try{
        const data=await categoryDetail.save()
        resp.json(data)
    }catch(err){
        resp.send(err)
    }
})

router.patch('/:id',async(req,resp)=>{
    // console.log('get req');
    // resp.send('get data')
    try{
        const category = await categorySch.findById(req.params.id)
        if(req.body.name){
            category.name = req.body.name
        }
        const data = await category.save();
        resp.json(data)

    }catch(err){
        resp.send(err)
    }
}) 

router.delete('/:id',async(req,resp)=>{
    console.log('get req',req.params.id);
    // resp.send('get data')
    try{
        const category = await categorySch.findById(req.params.id)
        console.log('category',category);
        const data = await category.remove();
        console.log(`user with this detail`,data);
        resp.json(`user with this detail`,data)
        // resp.status(200).json(data)

    }catch(err){
        resp.send(err)
    }
})

module.exports = router;