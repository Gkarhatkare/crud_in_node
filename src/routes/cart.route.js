const express = require('express');
const router = express.Router();
const cartSch = require('../model/cart.model')

console.log('cartSch',cartSch);


router.get('/',async(req,resp)=>{
    // console.log('get req');
    // resp.send('get data')
    try{
        const cart = await cartSch.find()
        console.log('get all productt', cart);
        resp.json(cart)

    }catch(err){
        resp.send(err)
    }
})  

router.post('/',async(req, resp)=>{
    // console.log('post req');
    console.log('req.body',req.body); 
    const cartDetail = new cartSch({
        user_id:req.body.user_id,
        product_id:req.body.product_id
    })
    console.log('cartDetail++++',cartDetail);
    try{
        const data=await cartDetail.save()
        resp.json(data)
    }catch(err){
        resp.send(err)
    }
})

router.patch('/:id',async(req,resp)=>{
    // console.log('get req');
    // resp.send('get data')
    try{
        const cartDetail = await cartSch.findById(req.params.id)
        if(req.body.product_id){
            cartDetail.product_id = req.body.product_id
        }
        const data = await cartDetail.save();
        resp.json(data)

    }catch(err){
        resp.send(err)
    }
}) 

router.delete('/:id',async(req,resp)=>{
    console.log('get req',req.params.id);
    // resp.send('get data')
    try{
        const cartDetail = await cartSch.findById(req.params.product_id)
        console.log('cartDetail',cartDetail);
        const data = await cartDetail.remove();
        resp.status(200).json(data)

    }catch(err){
        resp.send(err)
    }
})



module.exports = router;