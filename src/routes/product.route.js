const express = require('express');
const router = express.Router();
const productSch = require('../model/product.model')

console.log('productSch',productSch);


router.get('/',async(req,resp)=>{
    // console.log('get req');
    // resp.send('get data')
    try{
        const product = await productSch.find()
        console.log('get all productt', product);
        resp.json(product)

    }catch(err){
        resp.send(err)
    }
})  

router.post('/',async(req, resp)=>{
    const productDetail = new productSch({
        name:req.body.name,
        category:req.body.category,
        price:req.body.price,
        stock:req.body.stock,
    })
    console.log('productDetail++++',productDetail);
    try{
        const data=await productDetail.save()
        resp.json(data)
    }catch(err){
        resp.send(err)
    }
})

router.patch('/:id',async(req,resp)=>{
    // console.log('get req');
    // resp.send('get data')
    try{
        const product = await productSch.findById(req.params.id)
        if(req.body.name){
            product.name = req.body.name
        }else if(req.body.category){
            product.category = req.body.category
        }else if(req.body.stock){
            product.stock = req.body.stock
        } else if(req.body.price){
            product.price = req.body.price
        }
        const data = await product.save();
        resp.json(data)

    }catch(err){
        resp.send(err)
    }
}) 

router.delete('/:id',async(req,resp)=>{
    console.log('get req',req.params.id);
    // resp.send('get data')
    try{
        const product = await productSch.findById(req.params.id)
        console.log('product',product);
        const data = await product.remove();
        console.log(`user with this detail`,data);
        resp.json(`user with this detail`,data)
        resp.status(200).json(data)

    }catch(err){
        resp.send(err)
    }
})

module.exports = router;