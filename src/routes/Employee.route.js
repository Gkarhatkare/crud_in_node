const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
// const { v4: uuidv4 } = require("uuid");
const employeeSch= require('../model/emploee.model')
const emploeesData = mongoose.model('employee')
const bcrypt = require('bcryptjs')
console.log('emploees',emploeesData);
const validateRegisterInput = require('../service/employee.service')

// function withoutProperty(obj, property) {  
//     const { [property]: unused, ...rest } = obj

//   return rest
// }

router.get('/',async(req,resp)=>{
    // console.log('get req');`
    // resp.send('get data')
    try{
        let employee = await employeeSch.find()
        // const data = withoutProperty(employee, 'name')
         employee.filter(emp=>{
            return  emp.password = ""
        })

        console.log( '???????');

        resp.json(employee)

    }catch(err){
        resp.send(err)
    }
})  
 
router.get('/:id',async(req,resp)=>{
    // console.log('get req');`
    // resp.send('get data')

    try{
        const employee = await employeeSch.findById(req.params.id)
        resp.json(employee)
    }catch(err){
        resp.send(err)
    }
}) 

router.post('/',async(req, resp)=>{
    // console.log('post req');
    // console.log('req.body',req.body); 
    const { errors, isValid } = validateRegisterInput(req.body);
    if(!isValid){
        resp.status(400).json(errors);
    }

    emploeesData.findOne({email:req.body.email}).then(emp =>{
        console.log('user',emp);
        if(emp){
            resp.status(400).json({email:"already exist"})
        }else{
            const newEmp = new employeeSch({
                name:req.body.name,
                email:req.body.email,
                mobile:req.body.mobile,
                tech:req.body.tech,
                password:req.body.password
            })

            bcrypt.genSalt(10, (err,salt)=>{
                bcrypt.hash(newEmp.password, salt, (err, hash)=>{
                    if(err) throw err;
                    newEmp.password = hash;
                    newEmp.save().then(emply => resp.json(emply)).catch(err=> resp.json(err))
                    
                    // newEmp.save()
                })
            })
        }
    })


})

router.patch('/:id',async(req,resp)=>{
    // console.log('get req');
    // resp.send('get data')
    try{
        const employee = await employeeSch.findById(req.params.id)
        if(req.body.name){
            employee.name = req.body.name

        }else if(req.body.tech){
            employee.tech = req.body.tech
        }else if(req.body.email){
            employee.email = req.body.email
        }
        const data = await employee.save();
        resp.json(data)

    }catch(err){
        resp.send(err)
    }
}) 

router.delete('/:id',async(req,resp)=>{
    // console.log('get req');
    // resp.send('get data')
    try{
        const employee = await employeeSch.findById(req.params.id)
        employee.name = req.body.name
        employee.tech = req.body.tech
        employee.email = req.body.tech
        const data = await employee.remove();
        resp.json(`user with this detail`,data)

    }catch(err){
        resp.send(err)
    }
})

module.exports = router;